# FinShare  ![GitHub stars](https://img.shields.io/github/stars/iamabhishek229313/finshare?style=social)![GitHub forks](https://img.shields.io/github/forks/iamabhishek229313/finshare?style=social) 

## Fin[Finance]Share[Sharing]

![GitHub pull requests](https://img.shields.io/github/issues-pr/iamabhishek229313/finshare) ![GitHub last commit](https://img.shields.io/github/last-commit/iamabhishek229313/finshare)  ![GitHub issues](https://img.shields.io/github/issues-raw/iamabhishek229313/finshare) [![Open Source Love](https://badges.frapsoft.com/os/v2/open-source.svg?v=103)](https://github.com/iamabhishek229313/finshare)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)


## [Download this application] : https://drive.google.com/drive/folders/1srT46npRCj0acA6IxdEseJw3zHnwqSaE?usp=sharing

## [GitHub link for screenshots] : https://github.com/iamabhishek229313/finshare


## ☄ Problem we are dealing with 

* Sharing your card with your family and friends.
* Sharing cards can have unexpected consequences due to lack of any limits or purpose.

## ⚛ How we solved

An app, where the cardholder can share their card to partner/family/other/ and can limit their their daily/monthly budgets and per-transaction limits. Permissions can be grantedto shared people for the requirement of consent approval from the cardholder.

## ⚒ Market Competition?

No direct competition. First in the industry

## 📚 Technical Details
```
Bloc-Pattern
Flutter SDK.
Firebase Cloustore
Firebase Auth
Google Cloud Services
```
## ⚛ Features we provide

```
* Manage all cards at one space
* Share your cards with loved ones
* Add daily, monthly and per transaction limits
* Get detailed spending reports for all users
```


## ⚒ Pull Request 
I welcome and encourage all pull requests. It usually will take me within 24-48 hours to respond to any issue or request.


## © License 
```
MIT License

Copyright (c) 2021 Abhishek Kumar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
